# Workforce Optimization Tool (WOT)

WOT is the solution of the coding challenge which you find in **docs/SPO_CodingChallengeBE_January_2019.pdf**.

# Setup

Clone the repository with the command:

```
git clone https://marcellomon@bitbucket.org/marcellomon/wot.git
```

Then move to the folder **wot** and run the application:

```
cd wot
./gradlew bootRun
```

# Usage

Once the application is running, open another shell and send a JSON request like the following:
```
curl -X POST -H "Content-Type: application/json" -d '{"rooms":[35,21,17],"senior":10,"junior":6}' http://localhost:8080/optimize
```
The answer will look like:
```
[{"senior":2,"junior":1},{"senior":1,"junior":3},{"senior":1,"junior":15}]
```

# Tests

To run the tests use the following command:

```
./gradlew test
```

# Notes

The coding challenge is about solving an integer linear programming (ILP) problem, which can generally be described as:

min cx  
Ax >= b  
x>=0  

Where "x" is a positive integer vector. "c" and "b" are vectors and "A" is a matrix. x>=0 are constrains.  
The goal is to find the vector x which minimize the function cx.

Given:
- x1 = number of seniors  
- x2 = number of juniors  
- a1 = senior capacity = 10  
- a2 = junior capacity = 6  
- b = rooms = 35  

The coding challenge problem can be then defined as:

min x1+x2  
10x1 + 6x2 >= 35  
With x1>=1 and x2>=0  

A mechanism to solve ILP problems is by enumerating all the integer values of x1 and x2 and finding the solution which minimize the function min x1+x2.  
A second mechanism is called Branch and Bound, but given the limited state space search (max rooms=100 and max structures=100) the "brute force" algorithm has been implemented.
