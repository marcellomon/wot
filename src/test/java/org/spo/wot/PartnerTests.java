package org.spo.wot;

import org.junit.Test;
import org.spo.wot.model.Partner;

public class PartnerTests {

    @Test(expected = IllegalArgumentException.class)
    public void negativeSeniorCapacityTest(){
        new Partner(-1,5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeJuniorCapacityTest(){
        new Partner(10,-5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void JuniorGreaterThanSeniorCapacityTest(){
        new Partner(10,20);
    }
}
