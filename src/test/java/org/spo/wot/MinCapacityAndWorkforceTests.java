package org.spo.wot;

import org.junit.Before;
import org.junit.Test;
import org.spo.wot.model.Partner;
import org.spo.wot.model.Workforce;

import static org.junit.Assert.*;

public class MinCapacityAndWorkforceTests {

    private WorkforceOptimizer wo;
    private final static int MAX_ROOMS=100;

    @Before
    public void setUp() {
        wo = new MinCapacityAndWorkforce(MAX_ROOMS);
    }

    @Test
    public void example1Test(){
        wo.setPartner(new Partner(10,6));

        Workforce workforce1 = wo.getWorkforceFor(35);
        Workforce workforce2 = wo.getWorkforceFor(21);
        Workforce workforce3 = wo.getWorkforceFor(17);


        assertEquals(3,workforce1.getSenior());
        assertEquals(1,workforce1.getJunior());

        assertEquals(1,workforce2.getSenior());
        assertEquals(2,workforce2.getJunior());

        assertEquals(2,workforce3.getSenior());
        assertEquals(0,workforce3.getJunior());
    }

    @Test
    public void example2Test(){
        wo.setPartner(new Partner(11,6));

        Workforce workforce1 = wo.getWorkforceFor(24);
        Workforce workforce2 = wo.getWorkforceFor(28);


        assertEquals(2,workforce1.getSenior());
        assertEquals(1,workforce1.getJunior());

        assertEquals(2,workforce2.getSenior());
        assertEquals(1,workforce2.getJunior());

    }

    @Test(expected = IllegalArgumentException.class)
    public void RoomsGreaterThanMaxRoomsTest(){
        wo.setPartner(new Partner(11,6));
        wo.getWorkforceFor(MAX_ROOMS+10);
    }

}
