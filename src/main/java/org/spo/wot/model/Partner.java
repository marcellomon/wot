package org.spo.wot.model;

public class Partner {
    private int seniorCapacity;
    private int juniorCapacity;

    public Partner(int seniorCapacity, int juniorCapacity) {
        if(seniorCapacity<2)
            throw new IllegalArgumentException("senior capacity cannot be less than 2");
        if(juniorCapacity<1)
            throw new IllegalArgumentException("junior capacity cannot be less than 1");
        if(seniorCapacity<=juniorCapacity)
            throw new IllegalArgumentException("senior capacity must be greater than junior capacity");

        this.seniorCapacity = seniorCapacity;
        this.juniorCapacity = juniorCapacity;
    }

    public int getSeniorCapacity() {
        return seniorCapacity;
    }

    public int getJuniorCapacity() {
        return juniorCapacity;
    }
}
