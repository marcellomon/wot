package org.spo.wot.model;

public class Workforce {
    private int senior;
    private int junior;

    public Workforce(){

    }

    public Workforce(int senior, int junior) {
        this.senior = senior;
        this.junior = junior;
    }

    public int getSenior() {
        return senior;
    }

    public void setSenior(int senior) {
        this.senior = senior;
    }

    public int getJunior() {
        return junior;
    }

    public void setJunior(int junior) {
        this.junior = junior;
    }

    @Override
    public String toString() {
        return "{" +
                "senior:" + senior +
                ", junior:" + junior +
                '}';
    }
}
