package org.spo.wot.model;

import java.util.Arrays;

public class PartnerJob {
    private int senior;
    private int junior;
    private int[] rooms;

    public PartnerJob() {
    }

    public Partner getPartner() {
        return new Partner(senior,junior);
    }

    public int[] getRooms() {
        return rooms;
    }

    public void setSenior(int senior) {
        this.senior = senior;
    }

    public void setJunior(int junior) {
        this.junior = junior;
    }

    @Override
    public String toString() {
        return "{" +
                "senior:" + senior +
                ", junior:" + junior +
                ", rooms:" + Arrays.toString(rooms) +
                '}';
    }
}
