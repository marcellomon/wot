package org.spo.wot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WotApplication {

	public static void main(String[] args) {
		SpringApplication.run(WotApplication.class, args);
	}

}

