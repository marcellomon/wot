package org.spo.wot;

import org.spo.wot.model.Partner;
import org.spo.wot.model.Workforce;

public class MinCapacityAndWorkforce implements WorkforceOptimizer{

    private Partner partner;
    private int maxRooms;

    MinCapacityAndWorkforce(int maxRooms){
        if(maxRooms<0)
            throw new IllegalArgumentException("max rooms must be > 0");
        this.maxRooms=maxRooms;
    }

    @Override
    public void setPartner(Partner partner) {
        this.partner=partner;
    }

    @Override
    public Workforce getWorkforceFor(int rooms) {
        if(rooms<=0)
            throw new IllegalArgumentException("rooms must be > 0");
        if(rooms>maxRooms)
            throw new IllegalArgumentException("rooms must be less than "+maxRooms);

        int bestWorkforce, bestZ, bestSenior, bestJunior;
        bestSenior=bestJunior=0;
        bestWorkforce=bestZ=maxRooms*10;
        // senior capacity cannot be < 2.
        // If senior capacity = 2, the maximum number of seniors will be 50 (maxRooms/2) if number of juniors is 0
        for (int senior =1; senior<maxRooms/2 ;senior++){

            for (int junior=0; junior<maxRooms ;junior++){

                int z = partner.getSeniorCapacity() * senior + partner.getJuniorCapacity() * junior;
                if(z >= rooms && z<bestZ && senior+junior<bestWorkforce){
                    bestZ=z;
                    bestWorkforce=junior+senior;
                    bestSenior=senior;
                    bestJunior=junior;
                }
            }
        }
        return new Workforce(bestSenior,bestJunior);
    }
}
