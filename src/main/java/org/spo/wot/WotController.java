package org.spo.wot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spo.wot.model.PartnerJob;
import org.spo.wot.model.Workforce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class WotController {

    @Autowired
    private WorkforceOptimizer workforceOptimizer;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/optimize")
    List<Workforce> optimize(@RequestBody PartnerJob partnerJob){
        logger.info("Receiving request: "+partnerJob);
        List<Workforce> result = new ArrayList<>();
        try{
            workforceOptimizer.setPartner(partnerJob.getPartner());
            Arrays.stream(partnerJob.getRooms()).forEach( room -> result.add(workforceOptimizer.getWorkforceFor(room)) );
        } catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        logger.info("Response: "+result);
        return result;
    }
}
