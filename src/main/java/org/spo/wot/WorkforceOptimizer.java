package org.spo.wot;

import org.spo.wot.model.Partner;
import org.spo.wot.model.Workforce;

public interface WorkforceOptimizer {
    void setPartner(Partner partner);
    Workforce getWorkforceFor(int rooms);
}
