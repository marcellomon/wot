package org.spo.wot;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WotConfiguration {

    @Value("${max.rooms:100}")
    private int maxRooms;

    @Bean WorkforceOptimizer workforceOptimizer(){
        return new MinCapacityAndWorkforce(maxRooms);
    }

}
